//
//  UIImage+LTM_Larger.m
//  Component
//
//  Created by 柯南 on 2021/11/16.
//

#import "UIImage+LTM_Larger.h"

@implementation UIImage (LTM_Larger)
+ (UIImage *)largerImage:(NSString *)imageName{
    return  [UIImage largerImage:imageName ImageType:@"png"];
}

+ (UIImage *)largerImage:(NSString *)imageName ImageType:(NSString *)type{
    if (!imageName || imageName.length < 1) {
        return  [[UIImage alloc] init];
    }
    if (!type || type.length < 1) {
        type = @"png";
    }
    NSString *filePath = [[NSBundle mainBundle] pathForResource:imageName ofType:type];
    
    return  [UIImage imageWithContentsOfFile:filePath];
}
@end
