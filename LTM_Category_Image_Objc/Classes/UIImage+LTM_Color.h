//
//  UIImage+LTM_Color.h
//  Component
//
//  Created by 柯南 on 2021/11/16.
//  图片 - 颜色

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (LTM_Color)
/**
 根据颜色生成纯色图片
 
 @param color 颜色
 */
+ (UIImage *)imageWithColor:(UIColor *)color;

/**
 取图片某一像素的颜色
 
 @param point 颜色点
 */
- (UIColor *)colorAtPixel:(CGPoint)point;

/**
 获得灰度图
 
 */
- (UIImage *)convertToGrayImage;
@end

NS_ASSUME_NONNULL_END
