//
//  LTMViewController.m
//  LTM_Category_Image_Objc
//
//  Created by coenen on 11/16/2021.
//  Copyright (c) 2021 coenen. All rights reserved.
//

#import "LTMViewController.h"
#import <LTM_Category_Image_Objc/UIImage+LTM_Color.h>
@interface LTMViewController ()

@end

@implementation LTMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    UIImage *image = [UIImage imageWithColor:[UIColor redColor]];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(100, 100, 100, 140)];
    imageView.image = image;
    [self.view addSubview:imageView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
